package com.topgroup.prueba.service.impl;

import com.topgroup.prueba.model.Cliente;
import com.topgroup.prueba.service.ClienteService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Override
	public List<Cliente> generarClientes(MultipartFile archivo) {
		// TODO Auto-generated method stub
		List<Cliente> listaClientes = new ArrayList<>();
		try {
			Workbook workbook = WorkbookFactory.create(archivo.getInputStream());
			Sheet sheet = workbook.getSheetAt(1);

			for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
				Row row = sheet.getRow(rowNum);

				// Inicializamos las celdas con sus tipos
				row.getCell(0).setCellType(CellType.NUMERIC);
				row.getCell(1).setCellType(CellType.STRING);

				row.getCell(3).setCellType(CellType.STRING);

				row.getCell(6).setCellType(CellType.STRING);
				row.getCell(7).setCellType(CellType.NUMERIC);
				row.getCell(8).setCellType(CellType.NUMERIC);
				// row.getCell(9).setCellType(CellType.STRING);

				// Guardamos los datos de las celdas
				int ID_cliente = Math.round(Float.parseFloat(row.getCell(0).toString()));
				String ID_NMBRE1_CLN = row.getCell(1).toString();

				String ID_NMBRE2_CLN;
				if (row.getCell(2) == null)
					ID_NMBRE2_CLN = null;
				else {
					row.getCell(2).setCellType(CellType.STRING);
					ID_NMBRE2_CLN = row.getCell(2).toString();
				}

				String ID_APLLDOS1_CLN = row.getCell(3).toString();

				String ID_APLLDOS2_CLN;
				if (row.getCell(4) == null)
					ID_APLLDOS2_CLN = null;
				else {
					row.getCell(4).setCellType(CellType.STRING);
					ID_APLLDOS2_CLN = row.getCell(4).toString();
				}

				long CLLAR_CLN = 0;
				if (row.getCell(5) == null) {
					System.out.println("No tiene telefono");
				} else {
					row.getCell(5).setCellType(CellType.STRING);
					CLLAR_CLN = Long.parseLong(row.getCell(5).toString());
				}

				String E_MAIL_CLN = row.getCell(6).toString();
				int ID_DPT = Math.round(Float.parseFloat(row.getCell(7).toString()));
				int ID_CIU = Math.round(Float.parseFloat(row.getCell(7).toString()));
				String DRCCION_CLN = row.getCell(9).toString();

				String BRRIO_CLN;
				if (row.getCell(10) == null)
					BRRIO_CLN = null;
				else
					BRRIO_CLN = row.getCell(10).toString();

				// Creamos el objeto Cliente y guardamos

				Cliente cliente = new Cliente(ID_cliente, ID_NMBRE1_CLN, ID_NMBRE2_CLN, ID_APLLDOS1_CLN,
						ID_APLLDOS2_CLN, CLLAR_CLN, ID_CIU, ID_DPT, DRCCION_CLN, E_MAIL_CLN, BRRIO_CLN);

				listaClientes.add(cliente);

			}

		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaClientes;
	}

}
