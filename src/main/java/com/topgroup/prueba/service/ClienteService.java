package com.topgroup.prueba.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.topgroup.prueba.model.Cliente;

public interface ClienteService {

	public List<Cliente> generarClientes(MultipartFile archivo);
	
}
