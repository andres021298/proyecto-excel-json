package com.topgroup.prueba.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.topgroup.prueba.model.Cliente;
import com.topgroup.prueba.service.ClienteService;

@RestController
public class ClienteController {

	@Autowired
	ClienteService clienteService;

	@GetMapping(value = "clientes", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Cliente>> generarClientes(@RequestParam("clientes") MultipartFile archivoClientes) {
		List<Cliente> listaClientes = clienteService.generarClientes(archivoClientes);
		return ResponseEntity.ok(listaClientes);
	}
}
