package com.topgroup.prueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoExcelJsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoExcelJsonApplication.class, args);
	}

}
